__author__ = 'leonmax'
from functools import cmp_to_key

class Solution:
    def largest_number(self, nums):
        if len(nums) == 0:
            return ""
        sorted_num_strs = sorted([str(i) for i in nums], key=cmp_to_key(lambda a, b: int(b+a)-int(a+b)))
        return "".join(sorted_num_strs) if not sorted_num_strs[0] == "0" else "0"

    # @param {integer[]} nums
    # @param {integer} target
    # @return {integer[]}
    def two_sum(self, nums, target):
        reversed_index = {}
        for i in range(len(nums)):
            reversed_index[nums[i]] = i
        for i in range(len(nums)):
            complement = target - nums[i]
            if complement in reversed_index and reversed_index[complement] != i:
                return [i+1, reversed_index[complement]+1]

if __name__ == "__main__":
    s = Solution()
    assert s.largest_number([3, 30, 34, 5, 9]) == "9534330"
    assert s.largest_number([0, 0]) == "0"
    assert s.largest_number([]) == ""
    assert s.largest_number([0, 1]) == "10"
    assert s.largest_number([824,938,1399,5607,6973,5703,9609,4398,8247]) == "9609938824824769735703560743981399"
    assert s.largest_number([999999998,999999997,999999999]) == "999999999999999998999999997"