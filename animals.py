def find_sets(animals, k):
    visited = set()
    for a in animals:
        if k == 1:
            yield [a]
        else:
            visited.add(a)
            for s in find_sets(animals - visited, k-1):
                yield [a] + s


'''
find permutation of
'''

def find_permutation(seq):
    result = set()
    if len(seq) == 1:
        return seq
    for i in range(len(seq)):
        subinput = seq[0:i] + seq[i+1:len(seq)]
        for subresult in find_permutation(subinput):
            result.add(seq[i] + subresult)
    return result


def test_find_permutation():
    print(find_permutation("abc"))
    print(find_permutation("aba"))
    print(find_permutation(""))


if __name__ == "__main__":
    print("print all combos\n")
    for combo in find_sets({'cat', 'dog', 'cow', 'pig', 'tiger'}, 3):
        print(" ".join(combo))

    test_find_permutation()
