__author__ = 'leonmax'

from functools import cmp_to_key

''' longest adjacent sequence
given abed
'''
def longest_adjacent_seq(arr):
    result = -1
    if not arr: return result
    pairs = sorted(zip(range(len(arr)), arr), key=cmp_to_key(sort_func))
    for i in range(len(pairs)-1):
        if pairs[i] < pairs[i+1]:
            result = max(result, abs(pairs[i][0] - pairs[i+1][0]))
    return result


def sort_func(a, b):
    return b[0] - a[0] if a[1] == b[1] else a[1] - b[1]


def longest_palindromes(arr):
    pass

def test_longest_positive():
    assert longest_palindromes("abbaccaddaccad") == "accaddacca"

def longest_positive(arr):
    pass

def test_longest_positive():
    arr = [3, -6, 18, -4, 7, -9, -11, 6, -12, 9, 12]
    assert longest_positive(arr) == [18, -4, 7, -9, -11, 6]


if __name__ == "__main__":
    import random
    import time
    start = time.time()
    # print(solution([1]))
    print(longest_adjacent_seq([1, 4, 7, 3, 3, 5]))
    print(longest_adjacent_seq([random.randint(-2147483647, 2147483648) for i in range(40000)]))
    # print(solution([]))
    print("%s ms" % ((time.time() - start) * 1000))
