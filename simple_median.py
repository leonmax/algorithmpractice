__author__ = 'leonmax'

# given two sorted array, find the median number of all.
def median(arr1, arr2):
    if not arr1 and not arr2:
        return None
    i = 0
    j = 0
    even = (len(arr1) + len(arr2)) % 2 == 0
    middle = int((len(arr1) + len(arr2)) / 2)
    count = 0
    last = 0
    while i < len(arr1) and j < len(arr2):
        if arr1[i] <= arr2[j]:
            if count == middle:
                return (arr1[i] + last)/2 if even else arr1[i]
            last = arr1[i]
            i += 1
            count += 1
        else:
            if count == middle:
                return (last + arr2[j])/2 if even else arr2[j]
            last = arr2[j]
            j += 1
            count += 1
    if i < len(arr1):
        i2 = i + middle - count
        if not even:
            return arr1[i2]
        elif middle - count == 1:
            return (last + arr1[i2])/2
        else:
            return (arr1[i2-1] + arr1[i2])/2
    if j < len(arr2):
        j2 = j + middle - count
        if not even:
            return arr2[j2]
        elif middle - count == 0:
            return (last + arr2[j2])/2
        else:
            return (arr2[j2-1] + arr2[j2])/2


# all number in arr1 < arr2 and middle is in arr2
assert median([1, 2], [3, 5, 7]) == 3
# all number in arr1 is empty
assert median([], [1, 2]) == 1.5
# all number in arr1 is evenly distribued
assert median([1, 3], [2, 4]) == 2.5
# all number in arr1 < arr2 and middle is in arr1
assert median([0, 1], [2]) == 1
# all number in arr1 < arr2 and middle is in arr1
assert median([0, 1, 3], [5]) == 2
