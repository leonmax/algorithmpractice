import urllib
import json
import pprint

# we need to persistant the cache, and check the cache for expiracy before reuse!!!!
cache = {}
queries = 0

def query_track(name):
    global cache
    global queries
    params = {"q" : name}
    url = "http://ws.spotify.com/search/1/track.json?" + urllib.urlencode(params)
    req = urllib.Request(url)
    msg = urllib.urlopen(req).read()
    json_msg = json.loads(msg)
    goal_track = None
    for track in json_msg["tracks"]:
        cache[track["name"].lower()] = track
        if track["name"].lower() == name.lower():
            goal_track = track
    queries += 1
    return goal_track

def get_track(name):
    if name in cache:
        return cache[name.lower()]
    else:
        track = query_track(name)
        return track

##### THIS IS THE CORE OF THE PROGRAM, AND THE HARDEST PART ####
def all_cutpoints(num):
    for cutnum in range(num):
        print("trying to cut into %s pieces" % cutnum)
        cutpoints = [0]
        # no cutpoint
        if cutnum == 0:
            yield cutpoints
        else:
            # initial cutpoints
            for _ in range(cutnum):
                cutpoints.append(0)
            cp_idx = 1
            while cp_idx > 0:
                # if it's the last cutpoint
                if cp_idx == len(cutpoints)-1:
                    # if the last cutpoint is not overflow
                    if cutpoints[cp_idx] <= num-2:
                        cutpoints[cp_idx] += 1
                        yield cutpoints
                    else:
                        cp_idx -= 1
                # if the cutpoint is not overflow
                elif cutpoints[cp_idx] <= num-cutnum+cp_idx-2:
                    cutpoints[cp_idx] += 1
                    # reset the next layer
                    cutpoints[cp_idx+1] = cutpoints[cp_idx]
                    cp_idx += 1
                else:
                    cp_idx -= 1

def combo(words):
    for cutpoints in all_cutpoints(len(words)):
        phrases = []
        for i in range(len(cutpoints)):
            if i < len(cutpoints) - 1:
                phrases.append(" ".join(words[cutpoints[i]:cutpoints[i+1]]))
            else:
                phrases.append(" ".join(words[cutpoints[i]:]))
#        print cutpoints, phrases
        yield phrases

def get_list(sentense):
    words = sentense.split()
    for phrases in combo(words):
        playlist = []
        nextcomb = False
        for name in phrases:
            track = get_track(name)
            if track == None:
                nextcomb = True
                break
            playlist.append(track)
        if not nextcomb:
            return playlist
    return None


def get_list2(sentense):
    return None

def main(sentense):
    import time
    global queries
    start = int(time.time())
    playlist = get_list(sentense)
    if playlist is not None:
        print("Result: the playlist is as following:")
        reformat = [{"name"    :t["name"],
                     "artists" :[a["name"] for a in t["artists"]],
                     "album"   :t["album"]["name"]
          } for t in playlist]
        pprint.pprint(reformat)
    else:
        print("Result: can't not make such a playlist")
    print("totally %s seconds with %s queries" % (int(time.time()) - start, queries))

if __name__ == "__main__":
    import sys
    sentense = " ".join(sys.argv[1:])
    main(sentense)
