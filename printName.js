function printName(node, depth){
  var list = [node];
  var newLayer = node;
  var layer = 0;
  while (layer < depth && list.length > 0) {
    var node = list.shift();
    if (node === newLayer){
      layer++;
      newLayer = null;
    }
    if (newLayer === null && node.children.length > 0)
      newLayer = node.children[0];
    console.log(node.label);
    if (node.children) list = list.concat(node.children);
  }
}
