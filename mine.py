import random
import sys
import itertools

__author__ = 'leonmax'

from collections import namedtuple

Point = namedtuple("Point", ["y", 'x'])


class EmptyMatrixException(Exception):
    pass


class DeadException(Exception):
    pass


class Node:
    def __init__(self, value=0, revealed=False):
        if isinstance(value, int):
            self.value = value
        else:
            if value == '*':
                self.value = -1
            elif value == '_':
                self.value = 0
            else:
                self.value = int(value)
        self.revealed = revealed

    def __repr__(self):
        return "Node(%s)" % self.value


class Board:
    def __init__(self, length=50, width=100, density=0.1):
        self._length = length
        self._width = width
        self._density = density
        self._matrix = None
        self.reset()

    def set_board(self, matrix):
        if not matrix or not matrix[0]:
            raise EmptyMatrixException("empty matrix")
        self._matrix = [[Node(v) for v in row] for row in matrix]

    def reset(self):
        self._matrix = [[Node() for _ in range(self._width)] for _ in range(self._length)]
        for p in (Point(y, x) for x in range(self._width) for y in range(self._length)):
            if random.random() < self._density:
                self.get(p).value = -1
                for p in Board._find_neighbors(self._matrix, p):
                    if self.get(p).value != -1:
                        self.get(p).value += 1

    def get(self, point):
        return self._matrix[point.y][point.x]

    def print_matrix(self, masked=True):
        for row in self._matrix:
            for node in row:
                if not masked or node.revealed:
                    if node.value == -1:
                        sys.stdout.write('*')
                    elif node.value == 0:
                        sys.stdout.write('_')
                    else:
                        sys.stdout.write(str(node.value))
                else:
                    sys.stdout.write('#')
            sys.stdout.write('\n')

    @staticmethod
    def _find_neighbors(matrix, point):
        one = [-1,0,1]
        neighbors = [Point(point.y+y, point.x+x) for y, x in itertools.product(one, one) if y != 0 or x != 0]
        for n in neighbors:
            if 0 <= n.y < len(matrix) and 0 <= n.x < len(matrix[0]):
                yield n

    def click(self, target):
        print("%s,%s -> %s" % (target.y, target.x, self.get(target).value))
        if self.get(target).value == -1:
            raise DeadException("dead")
        node_to_check = [target]
        visited = set(node_to_check)
        while node_to_check:
            p = node_to_check.pop(0)
            if self.get(p).value != -1:
                self.get(p).revealed = True
                if self.get(p).value == 0:
                    for p in Board._find_neighbors(self._matrix, p):
                        if p not in visited and not self.get(p).revealed:
                            visited.add(p)
                            node_to_check.append(p)

if __name__ == "__main__":
    matrix = [
        '__12*1____',
        '__1*321_11',
        '__113*422*',
        '____2***21',
        '____12321_'
    ]
    board = Board()
    # board.set_board(matrix)
    board.print_matrix(False)
    board.click(Point(2, 3))
    board.print_matrix()
    board.click(Point(3, 2))
    board.print_matrix()
    board.click(Point(1, 7))
    board.print_matrix()
    board.click(Point(4, 6))
    board.print_matrix()
    board.click(Point(3, 5))
    board.print_matrix()