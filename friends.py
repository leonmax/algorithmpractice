__author__ = 'leonmax'

from heapq import heappush, heappushpop

def get_friends(id):
    friends = {
        '1': {
            'friends':[
                {'id': '2'},
                {'id': '3'},
                {'id': '4'}
            ]
        },
        '2': {
            'friends':[
                {'id': '5'},
                {'id': '6'}
            ]
        },
        '3': {
            'friends':[
                {'id': '5'},
                {'id': '7'}
            ]
        },
        '4': {
            'friends':[
                {'id': '5'},
                {'id': '6'},
                {'id': '8'}
            ]
        }

    }
    return friends[id]

def get_friend_suggestions(user_id, k=10):
    friends_score = {}
    friend_id_set = get_friend_id_set(user_id)
    for friend_id in friend_id_set:
        friend_of_friend_id_set = get_friend_id_set(friend_id)
        for ff in friend_of_friend_id_set - friend_id_set - set([user_id]):
            friends_score[ff] = friends_score.setdefault(ff, 0) + 1
    score_heap = []
    for user_id, score in friends_score.items():
        if len(score_heap) < k:
            heappush(score_heap, (score, user_id))
        else:
            if score_heap[0][0] < score:
                heappushpop(score_heap, (score, user_id))
    return [u[1] for u in score_heap]


def get_friend_id_set(user_id):
    result = get_friends(user_id)
    friend_ids = [f['id'] for f in result['friends']]
    return set(friend_ids)


if __name__ == "__main__":
    #print(get_friend_id_set('1'))
    print(get_friend_suggestions('1', k=3))