# given object/config below, implement a strategy to merge these two dictionary
# so that for the duplicated key of these 2 object, d2 wins!
d1 = {
    'onlyInD1': 'value in d1',
    'valueInBoth': 'value in d1',
    'dictOnlyInD1': {'keyInD1': 'd1'},
    'dictOnlyInD2': 'value in d1',
    'dictInBoth': {
        'noneInD1': None,
        'noneInD2': 'd1',
        'keyInBoth': 'd1',
        "keyInD1": 'd1',
        "dictInD1": {'a': 1}
    }
}

d2 = {
    'onlyInD2': 'value in d2',
    'valueInBoth' : 'value in d2',
    'dictOnlyInD1': 'value in d2',
    'dictOnlyInD2': {'keyInD2': 'd2'},
    'dictInBoth': {
        'noneInD1': 'd2',
        'noneInD2': None,
        'keyInBoth': 'd2',
        'keyInD2': 'd2',
        'dictInD2': {'a': 1}
    }
}

# this is the expected object
expected = {
    "valueInBoth": "value in d2",
    "onlyInD2": "value in d2",
    "dictOnlyInD1": "value in d2",
    "onlyInD1": "value in d1",
    "dictInBoth": {
        'noneInD1': 'd2',
        'noneInD2': 'd1',
        "keyInD2": "d2",
        "keyInBoth": "d2",
        "keyInD1": "d1",
        'dictInD1': {'a': 1},
        'dictInD2': {'a': 1}
    },
    "dictOnlyInD2": {
        "keyInD2": "d2"
    }
}

# example solution
import copy
def merge(d1, d2):
    # completely replace d1 with d2
    if d2 is None:
        return copy.deepcopy(d1)
    if not isinstance(d1, dict) or not isinstance(d2, dict):
        return copy.deepcopy(d2)
    result = copy.deepcopy(d1)
    for key in d2:
        if key in result:
            result[key] = merge(result[key], d2[key])
        elif d2[key] is not None:
            result[key] = d2[key]
    return result


import unittest
class TestMergeObject(unittest.TestCase):
    def test_upper(self):
        merged = merge(d1, d2)
        self.assertEqual(merged, expected)

    def test_simple_cases(self):
        self.assertEqual(merge({'a': 1}, 2), 2)
        self.assertEqual(merge(2, {'a': 1}), {'a': 1})
        self.assertEqual(merge(None, {'a': 1}), {'a': 1})
        self.assertEqual(merge({'a': 1}, None), {'a': 1})

if __name__ == '__main__':
    unittest.main()