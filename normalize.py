__author__ = 'leonmax'


def normalize(obj):
    queue = [obj]

    def extract_obj(item):
        if isinstance(item, list):
            return [extract_obj(subitem) for subitem in item]
        elif isinstance(item, dict) and 'uuid' in item:
            queue.append(item)
            return item['uuid']
        else:
            return item

    output = []
    while queue:
        next = queue.pop(0)
        normalized_obj = {'uuid': next['uuid'], 'properties': {}}
        for key, item in next['properties'].items():
            normalized_obj['properties'][key] = extract_obj(item)
        output.append(normalized_obj)
    return output


if __name__ ==  "__main__":
    # INPUT
    testcase = {
        "uuid": "350f37e0-71fe-6f05-d633-f8a9785ebe10",
        "properties": {
            "driver": {
                "uuid": "46a1b516-da30-4846-bf78-7ef3b3b7e325",
                "properties": {
                    "firstName": "Rob",
                    "lastName": "Testington",
                    "email": "r+drivertest@uber.com",
                    "rating": 5,
                    "status": 1,
                    "partnerCompany": "Not A Real Company, Inc.",
                    "activeVehicle": {
                        "uuid": "011028ce-25ab-4684-838b-bbc196e6e17b",
                        "properties": {
                            "licensePlate": "RUSHNYC",
                            "licensePlateState": "Alabama",
                            "telemetry": {
                                "uuid": "480851e1-5b4b-467a-ac66-1cd9b3fa3028",
                                "properties": {
                                    "latitude": 40.69186665838097,
                                    "longitude": -73.98898620843853,
                                    "course": 10.43138611300805,
                                    "altitude": 10,
                                    "time": 1433875748.406512,
                                    "speed": 15,
                                    "horizontalAccuracy": 100,
                                    "verticalAccuracy": 100
                                }
                            }
                        }
                    }
                }
            },
            "offers": [],
            "activeJobs": [
                {
                    "uuid": "a1e7e371-d340-4c05-9724-fd0841dde51b",
                    "properties": {
                        "titleCopy": "Large Box",
                        "subtitleCopy": "Order #1232",
                        "status": 0,
                        "notes": [],
                        "waypoints": [
                            {
                                "uuid": "7b59245a-a538-4047-b6f4-68b7125611bd",
                                "properties": {
                                    "status": 0,
                                    "location": {
                                        "uuid": "dc222cf4-1307-a51d-95d3-423385a5719f",
                                        "properties": {
                                            "latitude": 40.692122,
                                            "longitude": -73.988607,
                                            "address": {
                                                "uuid": "57e4a8cc-c4bc-441d-b22d-63681878432a",
                                                "properties": {
                                                    "formattedAddressLines": [
                                                        {
                                                            "uuid": "8821f734-7ccc-487d-bb39-cbee18e3d7bf",
                                                            "properties": {
                                                                "lineCopy": "Fulton Street Mall "
                                                            }
                                                        },
                                                        {
                                                            "uuid": "a769122c-657d-4d33-867a-a5b63233fc85",
                                                            "properties": {
                                                                "lineCopy": " 409 Fulton St, Brooklyn, NY 11201"
                                                            }
                                                        }
                                                    ]
                                                }
                                            }
                                        }
                                    },
                                    "eta": 100.00,
                                    "tasks": [
                                        {
                                            "uuid": "7412edb2-6130-2f9c-beb5-0294b06b424b",
                                            "properties": {
                                                "taskType": 1,
                                                "titleCopy": "Pick up: envelope",
                                                "subtitleCopy": "Order #1232",
                                                "contactPhoneNumber": "+1 254-555-1234",
                                                "contactName": "Uber Test Account "
                                            }
                                        }
                                    ]
                                }
                            },
                            {
                                "uuid": "e3483348-3224-4fa9-97e9-a8347a0e8f98",
                                "properties": {
                                    "status": 0,
                                    "location": {
                                        "uuid": "3d6e7207-12a8-cc22-a3c2-30bcb3db5cd7",
                                        "properties": {
                                            "latitude": 40.752735799999996,
                                            "longitude": -74.0064301,
                                            "address": {
                                                "uuid": "33235afb-8515-46c4-9cbf-88f96e8f01f7",
                                                "properties": {
                                                    "formattedAddressLines": [
                                                        {
                                                            "uuid": "71ff465c-17d9-4c9d-870a-c61fbb976dea",
                                                            "properties": {
                                                                "lineCopy": "636 W 28th St "
                                                            }
                                                        },
                                                        {
                                                            "uuid": "bd5590b6-3674-443d-b882-4a09065f628d",
                                                            "properties": {
                                                                "lineCopy": " New York, NY 10001"
                                                            }
                                                        }
                                                    ]
                                                }
                                            }
                                        }
                                    },
                                    "eta": 1347.09,
                                    "tasks": [
                                        {
                                            "uuid": "7ad409a9-9e7c-931c-fbd5-858cf2e864ce",
                                            "properties": {
                                                "taskType": 2,
                                                "titleCopy": "Drop off: envelope",
                                                "subtitleCopy": "Order #1232",
                                                "contactPhoneNumber": None,
                                                "contactName": "Rob S"
                                            }
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                }
            ],
            "plan": {
                "uuid": "e7707b19-de93-61a4-f278-38702d087497",
                "properties": {
                    "currentWaypoint": "7b59245a-a538-4047-b6f4-68b7125611bd",
                    "waypoints": [
                        "7b59245a-a538-4047-b6f4-68b7125611bd",
                        "e3483348-3224-4fa9-97e9-a8347a0e8f98"
                    ]
                }
            },
            "messages": [],
            "experiments": []
        }
    }

    # EXPECTED OUTPUT

    expected_jsonstr = """
    [ { "properties" : { "activeJobs" : [ "a1e7e371-d340-4c05-9724-fd0841dde51b" ],
            "driver" : "46a1b516-da30-4846-bf78-7ef3b3b7e325",
            "experiments" : [  ],
            "messages" : [  ],
            "offers" : [  ],
            "plan" : "e7707b19-de93-61a4-f278-38702d087497"
          },
        "uuid" : "350f37e0-71fe-6f05-d633-f8a9785ebe10"
      },
      { "properties" : { "activeVehicle" : "011028ce-25ab-4684-838b-bbc196e6e17b",
            "email" : "r+drivertest@uber.com",
            "firstName" : "Rob",
            "lastName" : "Testington",
            "partnerCompany" : "Not A Real Company, Inc.",
            "rating" : 5,
            "status" : 1
          },
        "uuid" : "46a1b516-da30-4846-bf78-7ef3b3b7e325"
      },
      { "properties" : { "licensePlate" : "RUSHNYC",
            "licensePlateState" : "Alabama",
            "telemetry" : "480851e1-5b4b-467a-ac66-1cd9b3fa3028"
          },
        "uuid" : "011028ce-25ab-4684-838b-bbc196e6e17b"
      },
      { "properties" : { "altitude" : 10,
            "course" : 10.43138611300805,
            "horizontalAccuracy" : 100,
            "latitude" : 40.69186665838097,
            "longitude" : -73.98898620843853,
            "speed" : 15,
            "time" : 1433875748.406512,
            "verticalAccuracy" : 100
          },
        "uuid" : "480851e1-5b4b-467a-ac66-1cd9b3fa3028"
      },
      { "properties" : { "notes" : [  ],
            "status" : 0,
            "subtitleCopy" : "Order #1232",
            "titleCopy" : "Large Box",
            "waypoints" : [ "7b59245a-a538-4047-b6f4-68b7125611bd",
                "e3483348-3224-4fa9-97e9-a8347a0e8f98"
              ]
          },
        "uuid" : "a1e7e371-d340-4c05-9724-fd0841dde51b"
      },
      { "properties" : { "eta" : 100,
            "location" : "dc222cf4-1307-a51d-95d3-423385a5719f",
            "status" : 0,
            "tasks" : [ "7412edb2-6130-2f9c-beb5-0294b06b424b" ]
          },
        "uuid" : "7b59245a-a538-4047-b6f4-68b7125611bd"
      },
      { "properties" : { "address" : "57e4a8cc-c4bc-441d-b22d-63681878432a",
            "latitude" : 40.692121999999998,
            "longitude" : -73.988607000000002
          },
        "uuid" : "dc222cf4-1307-a51d-95d3-423385a5719f"
      },
      { "properties" : { "formattedAddressLines" : [ "8821f734-7ccc-487d-bb39-cbee18e3d7bf",
                "a769122c-657d-4d33-867a-a5b63233fc85"
              ] },
        "uuid" : "57e4a8cc-c4bc-441d-b22d-63681878432a"
      },
      { "properties" : { "lineCopy" : "Fulton Street Mall " },
        "uuid" : "8821f734-7ccc-487d-bb39-cbee18e3d7bf"
      },
      { "properties" : { "lineCopy" : " 409 Fulton St, Brooklyn, NY 11201" },
        "uuid" : "a769122c-657d-4d33-867a-a5b63233fc85"
      },
      { "properties" : { "contactName" : "Uber Test Account ",
            "contactPhoneNumber" : "+1 254-555-1234",
            "subtitleCopy" : "Order #1232",
            "taskType" : 1,
            "titleCopy" : "Pick up: envelope"
          },
        "uuid" : "7412edb2-6130-2f9c-beb5-0294b06b424b"
      },
      { "properties" : { "eta" : 1347.0899999999999,
            "location" : "3d6e7207-12a8-cc22-a3c2-30bcb3db5cd7",
            "status" : 0,
            "tasks" : [ "7ad409a9-9e7c-931c-fbd5-858cf2e864ce" ]
          },
        "uuid" : "e3483348-3224-4fa9-97e9-a8347a0e8f98"
      },
      { "properties" : { "address" : "33235afb-8515-46c4-9cbf-88f96e8f01f7",
            "latitude" : 40.752735799999996,
            "longitude" : -74.006430100000003
          },
        "uuid" : "3d6e7207-12a8-cc22-a3c2-30bcb3db5cd7"
      },
      { "properties" : { "formattedAddressLines" : [ "71ff465c-17d9-4c9d-870a-c61fbb976dea",
                "bd5590b6-3674-443d-b882-4a09065f628d"
              ] },
        "uuid" : "33235afb-8515-46c4-9cbf-88f96e8f01f7"
      },
      { "properties" : { "lineCopy" : "636 W 28th St " },
        "uuid" : "71ff465c-17d9-4c9d-870a-c61fbb976dea"
      },
      { "properties" : { "lineCopy" : " New York, NY 10001" },
        "uuid" : "bd5590b6-3674-443d-b882-4a09065f628d"
      },
      { "properties" : { "contactName" : "Rob S",
            "contactPhoneNumber" : null,
            "subtitleCopy" : "Order #1232",
            "taskType" : 2,
            "titleCopy" : "Drop off: envelope"
          },
        "uuid" : "7ad409a9-9e7c-931c-fbd5-858cf2e864ce"
      },
      { "properties" : { "currentWaypoint" : "7b59245a-a538-4047-b6f4-68b7125611bd",
            "waypoints" : [ "7b59245a-a538-4047-b6f4-68b7125611bd",
                "e3483348-3224-4fa9-97e9-a8347a0e8f98"
              ]
          },
        "uuid" : "e7707b19-de93-61a4-f278-38702d087497"
      }
    ]
    """
    import json
    import pprint

    expected = json.loads(expected_jsonstr)
    pprint.pprint(expected)
    pprint.pprint(normalize(testcase))