import random

__author__ = 'leonmax'

matrix = [
    '....XX..',
    'XX...XXX',
    '..XX.XXX',
    '........',
    'XX......'
]
#
# find the largest cluster of nodes who contact with each other
# {'start': (4, 0), 'size': 8}
#

def find_cluster(matrix):
    visited = set()
    largest_cluster = None
    if matrix:
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if (i, j) not in visited and matrix[i][j] == 'X':
                    cluster = neighbors(matrix, i, j, visited)
                    if not largest_cluster or len(largest_cluster) < len(cluster):
                        largest_cluster = cluster
    return largest_cluster


def neighbors(matrix, i, j, visited):
    if len(matrix) > i >= 0 and len(matrix[0]) > j >= 0 and (i, j) not in visited:
        visited.add((i, j))
        if matrix[i][j] == 'X':
            cluster = [(i, j)]
            cluster += neighbors(matrix, i+1, j, visited)
            cluster += neighbors(matrix, i-1, j, visited)
            cluster += neighbors(matrix, i, j+1, visited)
            cluster += neighbors(matrix, i, j-1, visited)
            return cluster
    return []

if __name__ == "__main__":
    print(find_cluster(matrix))
    new_matrix = [''.join(['X' if (random.random() > 0.5) else '.' for j in range(1000)]) for i in range(1000)]
    for row in new_matrix: print(row)
    cluster = find_cluster(new_matrix)
    print(cluster[0], len(cluster))
    assert find_cluster([]) is None

