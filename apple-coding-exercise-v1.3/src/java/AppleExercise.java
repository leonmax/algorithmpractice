import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Stack;

/**
 * Copyright (C) 2010 Apple Inc.
 *
 * Version 2.7
 *
 * Answer as many or as few questions as you choose.
 * Don't spend too much time on any one problem.
 */
public class AppleExercise {

    /**
     * BEFORE STARTING: please add your name below so that we know who you are.
     *
     * NAME: Yangming Huang
     * DATE: Oct 27th, 2012
     *
     */

    /**
     * Using {@link AppleExercise.Problem1.SampleCache} as a starting point, answer questions 1-3 by
     * creating 3 new inner classes within {@link AppleExercise.Problem1} below.
     */
    public static class Problem1 {

        public interface Cache {
            public void put(Object key, Object value);
            public Object get(Object key);
        }

        public static class SampleCache implements Cache {

            private Map map;

            public SampleCache() {
                map = new HashMap();
            }

            // I didn't add this SupressWarning Annotation, it comes with the original file?
            @SuppressWarnings("unchecked")
            public void put(Object key, Object value) {
                map.put(key, value);
            }

            public Object get(Object key) {
                return map.get(key);
            }
        }

        /**
         * Problem 1.1
         *
         * Re-implement {@link AppleExercise.Problem1.SampleCache} as a singleton which implements the Cache interface.
         * Do not use any @SuppressWarnings annotations, and ensure that your implementation compiles without warnings
         * under Java 1.6.
         * 
         * </br>*****************************************************</br>
         * 
         * Answer 1.1
         * 
         * It need to be final to prevent the subclass destroy the Singleton.
         *
         * Name it {@link AppleExercise.Problem1.SingletonCache}
         */
        public static final class SingletonCache extends SampleCache implements Cache {
            private SingletonCache() {
                super();
            }

            private static SingletonCache instance = new SingletonCache();

            public static SingletonCache getInstance(){
                return instance;
            }
        }

        /**
         * Problem 1.2
         *
         * The HashMap class backing the {@link AppleExercise.Problem1.SampleCache} class is not thread-safe.
         * Duplicate and modify your {@link AppleExercise.Problem1.SingletonCache} implementation to ensure
         * that both Cache.put and Cache.get methods are safe to be called by multi-threads.
         * Do not use any @SuppressWarnings annotations, and ensure that your implementation compiles
         * without warnings under Java 1.6.
         *
         * </br>*****************************************************</br>
         * 
         * Answer 2.1
         * It's hard to test a ThreadSafe problem, especially for a final class that is hard to mock.
         * I need to write proxy in order to do that. I've attached a {@link ThreadSafeCacheTest}
         * to mimic the test anyway.
         * 
         * Name it {@link AppleExercise.Problem1.ThreadSafeCache}
         */
        public static final class ThreadSafeCache extends SampleCache implements Cache {
            private ThreadSafeCache() {
                super();
            }

            private static ThreadSafeCache instance = new ThreadSafeCache();

            public static ThreadSafeCache getInstance(){
                return instance;
            }

            @Override
            public synchronized void put(Object key, Object value) {
                super.put(key, value);
            }

            @Override
            public synchronized Object get(Object key) {
                return super.get(key);
            }
        }

        /**
         * Problem 1.3
         *
         * Use Java Generics to implement a non-singleton GenericCache class to improve compile-time type checking.
         *
         * Name it {@link AppleExercise.Problem1.GenericCache}
         */
        public static class GenericCache<K, V> {
            private Map<K, V> map;

            public GenericCache() {
                map = new HashMap<K, V>();
            }

            public void put(K key, V value) {
                map.put(key, value);
            }

            public V get(K key) {
                return map.get(key);
            }
        }
    }

    /**
     * Using any Java 1.6 classes or language features, implement these five methods.
     * Create any helper classes or methods that you think contribute to an elegant and
     * efficient solution.
     *
     * If you feel like there is more than one solution to the problem,
     * implement only the one that you feel is "best", and please explain
     * why you think it is a better choice than the alternatives.
     * List any assumptions that you made for your best solution.
     */
    public static class Problem2 {

        /**
         * Problem 2.1
         *
         * Return an Iterator that iterates through of each String in the order that they are provided,
         * but skips {@code null} values.
         * 
         * </br>*****************************************************</br>
         * 
         * Answer 2.1
         * I've wrote implementation of that simply through the array,
         * see {@link ArrayIterator}
         *
         * @param strings an array of nullable values
         * @return an iterator of strings
         * 
         */
        public static Iterator<String> iterateStrings(String... strings) {
            return new ArrayIterator<String>(strings);
        }

        /**
         * Problem 2.2
         *
         * Return a single Iterator<String> that chains together the array of Iterator<String>s in the order
         * that they are provided, but skips {@code null} values.
         * 
         * </br>
         * *****************************************************
         * </br>
         * 
         * Answer 2.2
         * I've wrote implementation of nested iteration of array of the iterator, but finally decide to
         * write a generic iterators to cover all the problems, see Answer 2.5 for details.
         *
         * @param stringIterators an array of nullable {@code Iterator<String>}s of nullable {@code String}s
         * @return an {@link java.util.Iterator} of {@code String}s
         * 
         */
        public static Iterator<String> iterateStrings(Iterator<String>... stringIterators) {
            return new GenericIterator<String>(String.class, (Object[]) stringIterators);
        }

        /**
         * Problem 2.3
         *
         * Return an Iterator that iterates through strings of each String[] in the order that they are
         * provided, but skips {@code null} values.
         * 
         * </br>
         * *****************************************************
         * </br>
         * 
         * Answer 2.3
         * I've wrote implementation of nested iteration of two of the arrays, but finally decide to
         * write a generic iterators to cover all the problems, see Answer 2.5 for details.
         *
         * @param stringArrays an array of nullable {@code String[}s of nullable {@code String}s
         * @return an {@link java.util.Iterator} of {@code String}s
         * 
         */
        public static Iterator<String> iterateStrings(String[]... stringArrays) {
            return new GenericIterator<String>(String.class, (Object[]) stringArrays);
        }

        /**
         * Problem 2.4
         *
         * Return an Iterator that iterates through all Integers within each two-dimensional Integer array in
         * the order that they are provided, but skips {@code null} values.
         * 
         * </br>
         * *****************************************************
         * </br>
         * 
         * Answer 2.4
         * I've wrote implementation of nested iteration of three of the arrays, but finally decide to
         * write a generic iterators to cover all the problems, see Answer 2.5 for details.
         *
         * @param twoDimensionalIntArrays an array of nullable {@code Integer[][]}s of nullable values
         * @return an {@link java.util.Iterator} of {@code Integer}s
         * 
         */
        public static Iterator<Integer> iterateInts(Integer[][]... twoDimensionalIntArrays) {
            return new GenericIterator<Integer>(Integer.class, (Object[]) twoDimensionalIntArrays);
        }

        /**
         * Problem 2.5
         *
         * Return an Iterator that iterates through all Integers within each set in the order that they are
         * provided, but skips {@code null} values.
         * 
         * </br>
         * *****************************************************
         * </br>
         *
         * Answer 2.4
         * I've wrote implementation of nested iteration of three of the arrays, but finally decide to
         * write a generic iterators to cover all the problems.
         * 
         * The GenericIterator use a stack to record the path, and traverse through the tree of the
         * Collection object passed in. When encounter a instance of the given type (passed in when Construct), return it,
         * else look for {@link java.util.Iterable} or {@link java.util.Iterator} or Arrays,
         * if none of those applied, it is a unexpected type, thus a {@link java.lang.ClassCastException}
         * will be thrown. 
         * 
         * Please see {@link GenericIterator}
         *
         * @param integerSetList a list of nullable {@code LinkedHashSet}s of nullable {@code Integer}s
         * @return an {@link java.util.Iterator} of {@code Integer}s
         *
         */
        public static Iterator<Integer> iterateInts2(List<LinkedHashSet<Integer>> integerSetList) {
            return new GenericIterator<Integer>(Integer.class, integerSetList);
        }

        /**
         * Problem 2.6
         *
         * Bonus question!
         *
         * Despite using generics, invoking one of the methods above in Problem 2 is not guaranteed to be
         * type-safe.
         *
         * Answer the following questions:
         *
         *   a) Which method is it?
         *
         *   b) Does the java compiler let you know? If not, how can you tell? If so, what warning or error
         *      will the compiler give you?
         *
         *   c) What can you do to avoid the problem?
         *
         * </br>
         * *****************************************************
         * </br>
         * 
         * Answer 2.6
         * 
         *   a) the method {@link #iterateStrings(Iterator...)}
         *   b) the java compiler will not let me know. but when using the method such as in
         *      {@link AppleExerciseTest#testIterateStrings2IteratorOfStringArray()}, the compiler will complain
         *      <i>"Type safety: A generic array of Iterator<String> is created for a varargs parameter"</i>
         *
         *   c) The reason for this is java arrays are covariant, but the Generic Types are not, so an array of
         *      GenericType will potential be first cast to the array of super class of GenericType and then cast
         *      to a different type of GenericParameter, thus not type safe any more.
         *      The only solution I can think of is to use instanceof to check type in runtime and throw Exception.
         *      I've also wrote a test {@link AppleExerciseTest#testIterateStringsIteratorOfStringArrayTypeSafe()}
         *      address this problem.
         */
    }

    /**
     * 
     * @author Yangming Huang
     *
     * A thin Iterator wrapper over an array. To be used by {@link AppleExercise.GenericIterator}
     *
     * @param <E> the Type of the Array.
     */
    public static class ArrayIterator<E> implements Iterator<E>{
        private final E[] array;
        private int nextPt = 0;

        public ArrayIterator(E[] array){
            this.array = array;
        }

        @Override
        public boolean hasNext() {
            for (;nextPt < array.length; nextPt++){
                if (array[nextPt] != null)
                    return true;
            }
            return false;
        }

        /**
         * better than the approach below:
         * <p><code>
         * try { <br/>&nbsp;&nbsp;&nbsp;&nbsp;
         *     return array[nextPt++]; <br/>
         * } catch (IndexOutOfBoundsException e) { <br/>&nbsp;&nbsp;&nbsp;&nbsp;
         *     throw new NoSuchElementException(); <br/>
         * } <br/>
         * </code></p>
         * 
         * From Effective Java, Joshua Bloch told us not to try and catch for predictable
         * exceptions, the exception mechanism is expensive and should only be used when
         * necessary.
         */
        @Override
        public E next() {
            if (hasNext()){
                return array[nextPt++];
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * 
     * @author Yangming Huang
     *
     * The GenericIterator use a stack to record the path, and traverse through the tree of the
     * Collection object passed in. When encounter a instance of the given type (passed in when Construct), return it,
     * else look for {@link java.util.Iterable} or {@link java.util.Iterator} or Arrays,
     * if none of those applied, it is a unexpected type, thus a {@link java.lang.ClassCastException}
     * will be thrown.
     * @param <E> The target Class to be iterated.
     * 
     */
    public static class GenericIterator<E> implements Iterator<E> {
        private Stack<Iterator<?>> iterStack;
        private E nextElem;
        private Class<E> clazz;

        /**
         * Construction for Iterator
         * 
         * @param clazz
         * @param iter any Iterator that is composed by Iterator, Iterable, Arrays or Class<E>
         */
        public GenericIterator(Class<E> clazz, Iterator<?> iter){
            initialize(clazz, iter);
        }

        /**
         * Construction for Iterable
         * 
         * @param clazz the expected Class
         * @param iter any Itarable that is composed by Iterator, Iterable, Arrays or Class<E>
         */
        public GenericIterator(Class<E> clazz, Iterable<?> iter){
            initialize(clazz, iter.iterator());
        }

        /**
         * Construction for anything else
         * 
         * @param clazz the expected Class
         * @param objs any object that is composed by Iterator, Iterable, Arrays or Class<E>
         */
        public GenericIterator(Class<E> clazz, Object... objs){
            Iterator<?> iter = (Iterator<?>) new ArrayIterator<Object>(objs);
            initialize(clazz, iter);
        }

        private void initialize(Class<E> clazz, Iterator<?> iter){
            this.iterStack = new Stack<Iterator<?>>();
            this.iterStack.push(iter);
            this.clazz = clazz;
        }

        @Override
        public boolean hasNext() {
            while (iterStack.size() > 0){
                if (nextElem !=null){
                    return true;
                }
                Iterator<?> iter = iterStack.peek();  
                if (iter.hasNext()){
                    Object obj = iter.next();
                    if (obj == null) {
                        continue;
                    } else if (obj.getClass() == clazz){
                        nextElem = clazz.cast(obj);
                        return true;
                    } else if (obj instanceof Object[]) {
                        Object[] arr = (Object[]) obj;
                        Iterator<?> deeper = (Iterator<?>) new ArrayIterator<Object>(arr);
                        iterStack.push(deeper);
                    } else if (obj instanceof Iterator) {
                        Iterator<?> deeper = Iterator.class.cast(obj);
                        iterStack.push(deeper);
                    } else if (obj instanceof Iterable) {
                        Iterator<?> deeper = Iterable.class.cast(obj).iterator();
                        iterStack.push(deeper);
                    } else {
                        clazz.cast(obj);
                    }
                } else {
                    iterStack.pop();
                }
            }
            return false;
        }

        @Override
        public E next() {
            if (hasNext() && nextElem!=null){
                E ret = nextElem;
                nextElem = null;
                return ret;
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}
