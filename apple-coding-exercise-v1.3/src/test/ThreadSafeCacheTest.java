import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Assert;
import org.junit.Test;

final class ThreadSafeSlowCache extends AppleExercise.Problem1.SampleCache implements AppleExercise.Problem1.Cache {

    private static ThreadSafeSlowCache instance = new ThreadSafeSlowCache();

    public static ThreadSafeSlowCache getInstance(){
        return instance;
    }

    @Override
    public synchronized void put(Object key, Object value) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        super.put(key, value);
    }

    @Override
    public synchronized Object get(Object key) {
        return super.get(key);
    }
}

final class CacheAccessor {
    private static AppleExercise.Problem1.ThreadSafeCache cache
        = AppleExercise.Problem1.ThreadSafeCache.getInstance();

    public static Runnable putter (final Integer key, final String value){
        return new Runnable(){
            @Override public void run(){ cache.put(key, value); }
        }; 
    }

    public static Callable<String> getter(final Integer key){
        return new Callable<String>(){
            @Override public String call(){ return (String) cache.get(key); }
        };
    }
}


/**
 * 
 * @author Yangming Huang
 *
 */
public final class ThreadSafeCacheTest {

    @Test
    public void testThreadSafeCache(){
        Integer key = 2012;
        String value = "Apple is so cool!";
        ExecutorService es = Executors.newCachedThreadPool();
        es.submit(CacheAccessor.putter(key, value));
        Future<String> f = Executors.newSingleThreadExecutor().submit(CacheAccessor.getter(key));
        try {
            Assert.assertEquals("get before/while put value", value, f.get());
        } catch (InterruptedException e) {
            Assert.fail("execution of CacheGetter interrupted");
        } catch (ExecutionException e) {
            Assert.fail("execution of CacheGetter failed"); 
        }
    }
}
