import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Assert;
import org.junit.Test;

/**
 * Copyright (C) 2010 Apple Inc.
 *
 * Add unit tests that will help verify that your implementation
 * for each problem is correct.
 *
 * Use junit annotations to identify your test methods.
 */
public final class AppleExerciseTest {

    @Test
    public void testSingletonCache() {
        AppleExercise.Problem1.SingletonCache cache1 = 
                AppleExercise.Problem1.SingletonCache.getInstance();
        AppleExercise.Problem1.SingletonCache cache2 = 
                AppleExercise.Problem1.SingletonCache.getInstance();
        Assert.assertSame("items from SingletonCache does not refer to the same object", cache1, cache2);

        Object o1 = new Object();
        Point o2 = new Point(1,2);
        cache1.put(1, o1);
        cache1.put(2, o2);
        Assert.assertSame("o1 should be the same as cache1.get(1)", o1, cache1.get(1));
        Assert.assertSame("o1 should be the same as cache2.get(1)", o1, cache2.get(1));
        Assert.assertSame("o2 should be the same as cache1.get(2)", o2, cache1.get(2));
        Assert.assertSame("o2 should be the same as cache2.get(2)", o2, cache2.get(2));
    }

    @Test
    public void testGenericCache() {
        AppleExercise.Problem1.GenericCache<Integer, Point> cache1 =
                new AppleExercise.Problem1.GenericCache<Integer, Point>();
        Point o1 = new Point(1,1);
        Point o2 = new Point(1,1);

        // test cache functionality
        cache1.put(1, o1);
        Point r1 = cache1.get(1);
        Assert.assertSame("cache1.get(1) should return o1", o1, r1);
        cache1.put(1, o2);
        Point r2 = cache1.get(1);
        Assert.assertSame("cache1.get(1) returns r2 == o2", o2, r2);
        Assert.assertEquals("cache1.get(1) should be logically equal to o1", o1, r2);
        Assert.assertNotSame("cache1.get(1)) should not be the same as o1", o1, r2);

        // other Generic Test
        AppleExercise.Problem1.GenericCache<Integer, Serializable> cache2 =
                new AppleExercise.Problem1.GenericCache<Integer, Serializable>();
        cache2.put(1, o1);
        Serializable s1 = cache2.get(1);
        Assert.assertSame("the Serializable s1 should refer to the same obj as o1",
                o1, s1);
    }

    private <E> void assertIterator(E[] expected, Iterator<E> iter){
        int i = 0;
        try {
            while (iter.hasNext()){
                iter.hasNext(); // check if hasNext is consistent no matter how many time being called.
                Assert.assertEquals(expected[i++], iter.next());
            }
        } catch (IndexOutOfBoundsException e) {
            Assert.fail("iterator has more elements than expected");
        }
        Assert.assertEquals("iterator has less elements than expected", expected.length, i);
        try {
            iter.next();
            Assert.fail("iterator is inconsitent, more from next() while hasNext() returns false");
        } catch (NoSuchElementException e){
        }
    }

    @Test
    public void testIterateStringsStringArray() {
        String[] expected = {"a", "b", "c"};
        Iterator<String> iter = AppleExercise.Problem2.iterateStrings(null, "a", "b", null, "c", null);

        assertIterator(expected, iter);
    }

    @Test
    public void testIterateStringsIteratorOfStringArray() {
        // test cases
        Iterator<String> subiter1 = Arrays.asList(null, "a", "b", null, "c", null).iterator();
        Iterator<String> subiter2 = Arrays.asList(new String[]{}).iterator();
        Iterator<String> subiter3 = Arrays.asList(null, "d", "e", null, "f", null).iterator();

        String[] expected = {"a", "b", "c", "d", "e", "f"};
        Iterator<String> iter = AppleExercise.Problem2.iterateStrings(subiter1, subiter2, null, subiter3);

        assertIterator(expected, iter);
    }

    @Test
    /**
     * test mentioned in Answer to Problem 2.6!
     */
    public void testIterateStringsIteratorOfStringArrayTypeSafe() {
        // test cases
        Iterator<Integer> intIter = Arrays.asList(1).iterator();

        // nothing will be complained on runtime
        Iterator<String> strIter = AppleExercise.Problem2.iterateStrings((Iterator<String>) (Object) intIter);

        // the iterator returned is marked as Iterator<String> but next() will return a Integer
        try {
            String a = strIter.next();
            Assert.fail("ClassCastException expected");
            Assert.assertNull(a);
        } catch (NoSuchElementException e){
            Assert.fail("iterator suppose to have 1 element");
        } catch (ClassCastException e) {
            // java.lang.Integer cannot be cast to java.lang.String
        }
    }

    @Test
    public void testIterateStringsStringArrayArray() {
        // test cases
        String[] arr1 = {null, "a", "b", null, "c", null};
        String[] arr2 = {};
        String[] arr3 = new String[3];
        String[] arr4 = {null, "d", "e", null, "f", null};

        String[] expected = {"a", "b", "c", "d", "e", "f"};
        Iterator<String> iter = AppleExercise.Problem2.iterateStrings(arr1, arr2, null, arr3, arr4);
        assertIterator(expected, iter);
    }

    @Test
    public void testIterateInts() {
        Integer[][] iMtx1 = {{11},{21,22}};
        Integer[][] iMtx2 = {{}};
        Integer[][] iMtx3 = new Integer[2][2];
        Integer[][] iMtx4 = {{null, 32},{41,null}};

        Integer[] expected = {11, 21, 22, 32, 41};
        Iterator<Integer> iter = AppleExercise.Problem2.iterateInts(iMtx1, iMtx2, null, iMtx3, iMtx4);

        assertIterator(expected, iter);

        ////////////////////////////////////////////////////////////////////////////
        int total = 1000000;
        Integer[][][] iMtx5 = new Integer[100][100][100];

        Integer[] expected2 = new Integer[total];

        for (int i=0; i<100; i++){
            for (int j=0; j<100; j++){
                for (int k=0; k<100; k++){
                    int n = 100 * (100 * i + j) + k;
                    iMtx5[i][j][k] = n;
                    expected2[n] = n;
                }
            }
        }

        long start = System.currentTimeMillis();

        Iterator<Integer> iter2 = AppleExercise.Problem2.iterateInts(iMtx5);
        assertIterator(expected2, iter2);

        long elapsedTimeMillis = System.currentTimeMillis()-start;
        System.out.println("testIterateInts: " + elapsedTimeMillis);
    }

    @Test
    public void testIterateInts2() {
        int total = 100000;
        int setCapacity = 10000;
        List<LinkedHashSet<Integer>> integerSetList = new ArrayList<LinkedHashSet<Integer>>();

        Integer[] expected = new Integer[total];

        LinkedHashSet<Integer> curSet = new LinkedHashSet<Integer>();
        integerSetList.add(curSet);
        for (int i=0; i<total; i++){
            if (curSet.size() >= setCapacity){
                curSet = new LinkedHashSet<Integer>(setCapacity);
                integerSetList.add(curSet);
            }
            curSet.add(i);
            expected[i] = i;
        }

        long start = System.currentTimeMillis();

        Iterator<Integer> iter = AppleExercise.Problem2.iterateInts2(integerSetList);
        assertIterator(expected, iter);

        long elapsedTimeMillis = System.currentTimeMillis()-start;
        System.out.println("testIterateInts2: " + elapsedTimeMillis);
    }
}
