import static org.junit.Assert.assertEquals;

import org.junit.Test;

class Simple extends Thread{
	private boolean flag = false;
	public boolean get(){
		return flag;
	}
	public void flip(){
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
		this.flag = true;
	}
	public void run(){
		System.out.println(this.getClass().getSimpleName() + " flipping");
		this.flip();
		System.out.println(this.getClass().getSimpleName() + " flipped");
	}
}

class SyncdSimple extends Simple{
	public synchronized boolean get(){
		return super.get();
	}
	public synchronized void flip(){
		super.flip();
	}
}

class Checker extends Thread{
	private Simple s = null;
	public boolean flag;
	public Checker(Simple s){
		this.s = s;
	}
	public void run(){
		synchronized(this){
			System.out.println(this.getClass().getSimpleName() + " checking");
			this.flag = s.get();
			System.out.println(this.getClass().getSimpleName() + " checked");
	        notifyAll();
		}
	}
}

public class TestSynchronized {
	@Test
	public final void testNonSynchronized() throws InterruptedException {
		System.out.println("Test Unsynchronized");
		Simple s = new Simple();
		Checker c = new Checker(s);
		s.start();
		c.start();

	    synchronized (c) {
	        c.wait(2000);
	    }
		System.out.println("The observed value is " + c.flag);
		assertEquals("Not Flipped", false, c.flag);
	    System.out.println();
	}

	@Test
	public final void testSynchronized() throws InterruptedException {
		System.out.println("Test Synchronized");
		Simple s = new SyncdSimple();
		Checker c = new Checker(s);
		s.start();
		c.start();

	    synchronized (c) {
	        c.wait(2000);
	    }
		System.out.println("The observed value is " + c.flag);
		assertEquals("Flipped", true, c.flag);
	    System.out.println();
	}
}
