def qsort(arr, left=None, right=None):
    if not arr:
        return arr
    if left is None:
        left = 0
    if right is None:
        right = len(arr)-1
    if left >= right:
        return arr
    pvt = left
    for i in range(left, right):
        if arr[i] < arr[right]:
            swap(arr, pvt, i)
            pvt += 1
    swap(arr, pvt, right)
    qsort(arr, left, pvt-1)
    qsort(arr, pvt+1, right)
    return arr

def qsort2(arr, left=None, right=None):
    if not arr:
        return arr
    if not left:
        left = 0
    if not right:
        right = len(arr)-1
    if left >= right:
        return arr

def swap(arr, i1, i2):
    if i1 < 0 or i2 < 0 or i1 > len(arr) or i2 > len(arr) or i1 == i2:
        return arr
    temp = arr[i1]
    arr[i1] = arr[i2]
    arr[i2] = temp

if __name__ == "__main__":
    assert qsort([4, 2, 9, 3, 2, 8, 7]) == [2, 2, 3, 4, 7, 8, 9]
    assert qsort([1, 3, 4, 2, 7]) == [1, 2, 3, 4, 7]
    assert qsort([9, 8, 11, 7]) == [7, 8, 9, 11]
    assert qsort([7, 3, 7]) == [3, 7, 7]
