'''
Created on Sep 1, 2010

@author: leonmax
'''

from itertools import product
def mark0(mtx):
    rowmap = colmap = 0
    if not mtx or not mtx[0]:
        return mtx
    else:
        M = len(mtx)
        N = len(mtx[0])
    for r,c in product(range(M),range(N)):
        if mtx[r][c]==0:
            rowmap|=(1<<r)
            colmap|=(1<<c)
    for r,c in product(range(M),range(N)):
        if (rowmap>>r)&1==1 or (colmap>>c)&1==1:
            mtx[r][c] = 0
    return mtx

expect1 = [[1,0,1,0],[0,0,0,0],[0,0,0,0]]
case1 = [[1,1,1,1],[1,0,1,1],[1,1,1,0]]


def isSubstring(s1, s2):
    return s2 in s1

    def isRotation(s1, s2):
        if not s1 or not s2 or len(s1) != len(s2):
            return False
    return isSubstring(s1+s1, s2)
  
def findMaxSum(arr):
    if not arr:
        return None, None, None
    l = 0
    u = 0
    sum = 0
    maxl = 0
    return l, u, sum

testcase1 = "apple", "pleap"
testcase2 = None, "pleap"
testcase3 = "apple1", "pleap"


if __name__ == '__main__':
    assert isSubstring(testcase1, True)