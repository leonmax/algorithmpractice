'''
Created on Sep 19, 2010

@author: leonmax
'''

class LinkedListNode():
    data = None
    __next = None
    __prev = None
    
    def __init__(self,data):
        self.data = data
        self.first = self
        self.last = self
        
    def hasNext(self):
        if self.__next!=None:
            return True
        else:
            return False
        
    def hasPrev(self):
        if self.prev!=None:
            return True
        else:
            return False
    
    def first(self):
        return self.__first
    
    def last(self):
        return self.__last
    
    def next(self):
        return self.__next
    
    def prev(self):
        return self.__prev
    
    def append(self, data):
        node = self
        while node.__next!=None:
            node = node.__next
        next = node.__next = LinkedListNode(data)
        next.__prev = node
        
    def delete(self, node):
        p = self.first()
        if p == node:
            pass
        while (node.next()!=None):
            if node==node:
                pass
                

if __name__ == '__main__':
    list = LinkedListNode(0)
    for i in range(1,5):
        list.addNode(i)
    list = list.first
    while list.hasNext():
        print list.data
        list = list.__next
    print list.data
    pass