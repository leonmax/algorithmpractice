def qsort(L):
    if L == []: return []
    return qsort([x for x in L[1:] if x< L[0]]) + L[0:1] + \
          qsort([x for x in L[1:] if x>=L[0]])

if __name__ == '__main__':
    print qsort([3,4,2,8,3,7,4,9,2,3])
