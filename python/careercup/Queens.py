'''
Created on Sep 6, 2010

@author: leonmax
'''

class queensSolution:
    amount = 0 
    def smartQueens(self, row, s):
        n = len(s)
        if row == n:
            self.amount += 1
            self.printSolution(n, s)
        for col in range(n):
            def conflict(row, s):
                for i in range(row):
                    if col==s[i]\
                    or col==s[i]+row-i\
                    or col==s[i]-row+i:
                        return True
            if not conflict(row, s):
                s[row] = col
                self.smartQueens(row+1, s)
                
    def rot_right(self, matrix, times):
        t = times%4
        n = len(matrix)
        if t==0:
            return matrix
        elif t==1:
            return [[matrix[n-1-j][i] for j in range(n)] for i in range(n)]
        elif t==2:
            return [[matrix[n-1-i][n-1-j] for j in range(n)] for i in range(n)]
        else:
            return [[matrix[j][n-1-i] for j in range(n)] for i in range(n)]
    
    def queens(self, n, row, s):
        if row == n:
            self.amount += 1
            self.printSolution(n, s)
        for col in range(n):
            flag = True
            for i in range(row):
                if col==s[i]\
                or col==s[i]+row-i\
                or col==s[i]-row+i:
                    flag = False
                    break
            if flag == True:
                s[row] = col
                self.queens(n, row+1, s)
                    
        
    def printSolution(self, n, s):
        print 'solution:'
        
        for col in s:
            print '0 '*col+'1 '+'0 '*(n-col-1)
        print
            
    def fullTraverse(self): 
        pass
        
if __name__ == '__main__':
#    s = [-1 for r in range(8)]
#    
    qs =  queensSolution()
#    qs.smartQueens(0,s)
#    print qs.amount
    m = [[1,2,3],[4,5,6],[7,8,9]]
    print m
    m = qs.rot_right(m,1)
    print m
    for i in range(3):
        for j in range(3):
            print m[i][j],
        print
    
#    qs =  queensSolution()
#    qs.queens(8,0,s)
#    print qs.amount