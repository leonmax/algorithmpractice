import sys
import random
import math

__author__ = 'leonmax'


class Bucket:
    def __init__(self):
        self.numbers = []
        self.min = None
        self.max = None

    def put(self, num):
        if self.min is None or self.min > num:
            self.min = num
        if self.max is None or self.max < num:
            self.max = num
        self.numbers.append(num)

    def get(self, i):
        return self.numbers[i]

    @property
    def count(self):
        return len(self.numbers)

    def shuffle(self, pick_bucket):
        for num in self.numbers:
            pick_bucket(num).put(num)

    def __repr__(self):
        return self.numbers.__repr__()


def median(buckets, int_min=0, int_max=sys.maxsize):
    target = int(sum([b.count for b in buckets]) / 2)
    buckets = shuffle(buckets, len(buckets), int_min, int_max)
    while True:
        i, passed = find_bucket(buckets, target)
        if buckets[i].count == 1:
            return buckets[i].get(0)
        target -= passed
        buckets = shuffle([buckets[i]], len(buckets), buckets[i].min, buckets[i].max)


def shuffle(buckets, bucket_num, int_min, int_max):
    bucket_size = math.ceil((int_max + 1 - int_min) / bucket_num)
    new_buckets = [Bucket() for _ in range(bucket_num)]
    for i in range(len(buckets)):
        buckets[i].shuffle(lambda x: new_buckets[int((x-int_min)/bucket_size)])
    return new_buckets


def find_bucket(buckets, target):
    passed = 0
    for i in range(len(buckets)):
        if passed + buckets[i].count > target:
            return i, passed
        else:
            passed += buckets[i].count


if __name__ == "__main__":
    total = 100000
    int_min = 0
    int_max = sys.maxsize
    bucket_num = 1000
    buckets = [Bucket() for _ in range(bucket_num)]
    for b in buckets:
        for i in range(int(total/bucket_num)):
            b.put(random.randint(0, int_max))
    print(median(buckets, int_min, int_max))
